################## BEGIN INSTALLATION ######################
FROM ubuntu:16.04

# Install general dependencies
RUN apt update && \
 	apt install -y \
	software-properties-common \
	wget \
	apt-utils \
	sudo \
	psmisc \
	git \
	x11-apps

# setting up a user
# Replace 1000 with your user / group id
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer
RUN sudo ls /etc/sudoers.d/
RUN sudo chmod 0440 /etc/sudoers.d/developer && \
    sudo chown ${uid}:${gid} -R /home/developer

WORKDIR /
################## END INSTALLATION ######################

################## BEGIN CONFIGURING OAI ######################
# get the OAI 5g repo master branch
RUN GIT_SSL_NO_VERIFY=true \
 git clone https://gitlab.eurecom.fr/oai/openairinterface5g.git

WORKDIR openairinterface5g
# RUN ls
# RUN source oaienv
WORKDIR cmake_targets
# Build eNB
RUN ./build_oai --eNB -w USRP -I
WORKDIR /openairinterface5g/targets/bin
################## END CONFIGURING OAI ######################

#
USER developer
ENV HOME /home/developer
